//
//  City+CoreDataProperties.swift
//  
//
//  Created by Ольга Бычок on 10/1/19.
//
//

import Foundation
import CoreData

extension City {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: "City")
    }

    @NSManaged public var countryCode: String?
    @NSManaged public var timeZone: String?
    @NSManaged public var name: String?
    @NSManaged public var currentWeather: CurrentWeather?
    @NSManaged public var forecasts: NSOrderedSet?

}

// MARK: Generated accessors for forecasts
extension City {

    @objc(insertObject:inForecastsAtIndex:)
    @NSManaged public func insertIntoForecasts(_ value: Forecast, at idx: Int)

    @objc(removeObjectFromForecastsAtIndex:)
    @NSManaged public func removeFromForecasts(at idx: Int)

    @objc(insertForecasts:atIndexes:)
    @NSManaged public func insertIntoForecasts(_ values: [Forecast], at indexes: NSIndexSet)

    @objc(removeForecastsAtIndexes:)
    @NSManaged public func removeFromForecasts(at indexes: NSIndexSet)

    @objc(replaceObjectInForecastsAtIndex:withObject:)
    @NSManaged public func replaceForecasts(at idx: Int, with value: Forecast)

    @objc(replaceForecastsAtIndexes:withForecasts:)
    @NSManaged public func replaceForecasts(at indexes: NSIndexSet, with values: [Forecast])

    @objc(addForecastsObject:)
    @NSManaged public func addToForecasts(_ value: Forecast)

    @objc(removeForecastsObject:)
    @NSManaged public func removeFromForecasts(_ value: Forecast)

    @objc(addForecasts:)
    @NSManaged public func addToForecasts(_ values: NSOrderedSet)

    @objc(removeForecasts:)
    @NSManaged public func removeFromForecasts(_ values: NSOrderedSet)

}
