//
//  CurrentWeather+CoreDataProperties.swift
//  
//
//  Created by Ольга Бычок on 10/1/19.
//
//

import Foundation
import CoreData
import UIKit

extension CurrentWeather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CurrentWeather> {
        return NSFetchRequest<CurrentWeather>(entityName: "CurrentWeather")
    }

    @NSManaged public var cloud: Int64
    @NSManaged public var code: Int64
    @NSManaged public var currentTemp: Int64
    @NSManaged public var feelsLike: Double
    @NSManaged public var humidity: Int64
    @NSManaged public var isDay: String?
    @NSManaged public var precipitation: Double
    @NSManaged public var pressure: Double
    @NSManaged public var sunrise: String?
    @NSManaged public var sunset: String?
    @NSManaged public var uvIndex: Int64
    @NSManaged public var visibility: Double
    @NSManaged public var weatherDescription: String?
    @NSManaged public var windDirection: String?
    @NSManaged public var windSpeed: Double
    @NSManaged public var city: City?

    func addImage(to element: UIImageView, elementIsIcon: Bool, code: Int64) {
        var addToName: String
        if elementIsIcon {
            addToName = "Icon"
        } else {
        if isDay == "d" {
            addToName = ""
        } else {
            addToName = "night"
        }
        }
        switch code {
        case 300, 301, 302:
            element.image = UIImage(named: addToName + "frost")
        case 230, 231, 232, 233:
            element.image = UIImage(named: addToName + "thunderstorm")
        case 700, 711, 721, 731, 741, 751:
            element.image = UIImage(named: addToName + "fog")
        case 801, 802, 803, 804:
            element.image = UIImage(named: addToName + "clouds")
        case 800:
            element.image = UIImage(named: addToName + "clearSky")
        case 600, 601, 602, 610, 611, 612, 621, 622, 623:
            element.image = UIImage(named: addToName + "snow")
        default:
            element.image = UIImage(named: addToName + "rain")
        }
    }

}
