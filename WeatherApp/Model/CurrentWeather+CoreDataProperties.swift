//
//  CurrentWeather+CoreDataProperties.swift
//  
//
//  Created by Ольга Бычок on 9/21/19.
//
//

import Foundation
import CoreData
import UIKit

extension CurrentWeather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CurrentWeather> {
        return NSFetchRequest<CurrentWeather>(entityName: "CurrentWeather")
    }

    @NSManaged public var currentTemp: String
    @NSManaged public var weatherDescription: String?
    @NSManaged public var humidity: Int16
    @NSManaged public var isDay: Int16
    @NSManaged public var pressure: Double
    @NSManaged public var windDirection: String?
    @NSManaged public var windSpeed: Double
    @NSManaged public var feelsLike: Double
    @NSManaged public var visibility: Double
    @NSManaged public var precipitation: Double
    @NSManaged public var uvIndex: Int16
    @NSManaged public var cloud: Int16
    @NSManaged public var code: Int16
    @NSManaged public var city: City?
    @NSManaged public var sunrise: String?
    @NSManaged public var sunset: String?

    func addImage(to element: UIImageView) {
        var addToName: String
        if isDay == 1 {
            addToName = ""
        } else {
            addToName = "night"
        }
        switch code {
        case 1072, 1171:
            element.image = UIImage(named: addToName + "frost")
        case 1087:
            element.image = UIImage(named: addToName + "thunderstorm")
        case 1030, 1135, 1147:
            element.image = UIImage(named: addToName + "fog")
        case 1003, 1004, 1005, 1006, 1007, 1008, 1009:
            element.image = UIImage(named: addToName + "clouds")
        case 1000:
            element.image = UIImage(named: addToName + "clearSky")
        default:
            element.image = UIImage(named: "rain")
        }
    }

}
