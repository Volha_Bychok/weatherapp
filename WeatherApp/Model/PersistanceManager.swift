//
//  PersistanceManager.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 9/22/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import CoreData

class PersistanceManager {

    // MARK: Properties

    static let shared = PersistanceManager()

    // MARK: Methods

    private init() {
    }

    // MARK: Create entity

    func createWeatherData(from jsonWeatherData: [String: Any], and forecast: [[String: Any]]) {
        guard let dataArray = jsonWeatherData["data"] as? [[String: Any]] else {return}
        guard let data = dataArray.first else {return}
        guard let weather =  data["weather"] as? [String: Any] else {return}
        guard let currentCode = weather["code"] as? String,
            let code = Int64(currentCode),
            let temp = data["temp"] as? Double,
            let humidity = data["rh"] as? Int64,
            let cloud = data["clouds"] as? Int64,
            let feelslike = data["app_temp"] as? Double,
            let windKph = data["wind_spd"] as? Double,
            let pressure = data["pres"] as? Double,
            let precipitation = data["precip"] as? Double,
            let visibility = data["vis"] as? Double,
            let uvIndex = data["uv"] as? Double
            else {return}
        let name = data["city_name"] as? String
        let countryCode = data["country_code"] as? String
        let weatherData: City? = findCityData(by: name, and: countryCode) ?? createCityData(by: name, and: countryCode)
        weatherData?.timeZone = data["timezone"] as? String
        let currentWeather = NSEntityDescription.insertNewObject(forEntityName: "CurrentWeather", into: context) as? CurrentWeather
        currentWeather?.code = code
        let tempInt = Int64(temp)
        let currentTemp = degreeСonversion(from: tempInt, isInitialConversion: true)
        currentWeather?.currentTemp = currentTemp
        currentWeather?.weatherDescription = weather["description"] as? String
        currentWeather?.isDay = data["pod"] as? String
        currentWeather?.feelsLike = feelslike
        currentWeather?.humidity = humidity
        currentWeather?.precipitation = precipitation
        currentWeather?.pressure = pressure
        currentWeather?.uvIndex = Int64(uvIndex)
        currentWeather?.visibility = visibility
        currentWeather?.windSpeed = windKph
        currentWeather?.cloud = cloud
        currentWeather?.windDirection = data["wind_cdir"] as? String
        weatherData?.currentWeather = currentWeather
        for dayForecast in forecast {
            guard let oneDayForecast = NSEntityDescription.insertNewObject(forEntityName: "Forecast", into: context) as? Forecast,
                let forecastWeather = dayForecast["weather"] as? [String: Any],
                let maxTemp = dayForecast["max_temp"] as? Double,
                let minTemp = dayForecast["min_temp"] as? Double,
                let code = forecastWeather["code"] as? Int64
                else {return}
            let maxTempInt = Int64(round(maxTemp))
            let minTempInt = Int64(round(minTemp))
            let forecastMaxTemp = degreeСonversion(from: maxTempInt, isInitialConversion: true)
            let forecastMinTemp = degreeСonversion(from: minTempInt, isInitialConversion: true)
            oneDayForecast.maxTemp = forecastMaxTemp
            oneDayForecast.minTemp = forecastMinTemp
            currentWeather?.sunset = data["sunset"] as? String
            currentWeather?.sunrise = data["sunrise"] as? String
            oneDayForecast.code = code
            weatherData?.addToForecasts(oneDayForecast)
        }
        saveContext()
    }

    func createCityData(by cityName: String?, and countryCode: String?) -> City? {
        guard let name = cityName,
            let countryCode = countryCode
            else {return nil}
        let weatherData = NSEntityDescription.insertNewObject(forEntityName: "City", into: context) as? City
        weatherData?.name = name
        weatherData?.countryCode = countryCode
        return weatherData
    }

    func findCityData(by cityName: String?, and countryCode: String?) -> City? {
        guard let name = cityName,
            let countryCode = countryCode
            else {return nil}
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        let namePredicate = NSPredicate(format: "name = %@", name)
        let countryPredicate = NSPredicate(format: "countryCode = %@", countryCode)
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [namePredicate, countryPredicate])
        do {
            let cities = try context.fetch(request) as? [City]
            return cities?.first
        } catch let error {
            print(error)
        }
        return nil
    }

    // MARK: Update entity

    func changeWeatherTemperatureData() {
        let cities = fetchWeatherData()
        for city in cities {
            guard let value = city.currentWeather?.currentTemp else {return}
            let currentTemp = degreeСonversion(from: value, isInitialConversion: false)
            city.currentWeather?.setValue(currentTemp, forKey: "currentTemp")
        }
        let forecasts = fetchForecastData()
        for forecast in forecasts {
            let maxTemp = forecast.maxTemp
            let minTemp = forecast.minTemp
            let forecastMaxTemp = degreeСonversion(from: maxTemp, isInitialConversion: false)
            let forecastMinTemp = degreeСonversion(from: minTemp, isInitialConversion: false)
            forecast.maxTemp = forecastMaxTemp
            forecast.minTemp = forecastMinTemp
        }
        saveContext()
        DataManager.shared.sendNotificationForUpdateTableView()
    }

    func degreeСonversion(from value: Int64, isInitialConversion: Bool) -> Int64 {
        let value = Double(value)
        var currentTemp: Int64
        if UserDefaults.standard.bool(forKey: "Celsius") || UserDefaults.standard.object(forKey: "Celsius") == nil {
            if isInitialConversion {
                currentTemp = Int64(value)
            } else {
                currentTemp = Int64(round((value - 32) * 5/9))
            }
        } else {
            currentTemp = Int64(round((value * 9/5) + 32))
        }
        return currentTemp
    }

    // MARK: Fetch data

    func fetchWeatherData() -> [City] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        do {
            let items = try context.fetch(fetchRequest) as? [City]
            return items ?? []
        } catch let error {
            print(error)
        }
        return []
    }

    func fetchForecastData() -> [Forecast] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Forecast")
        do {
            let items = try context.fetch(fetchRequest) as? [Forecast]
            return items ?? []
        } catch let error {
            print(error)
        }
        return []
    }

    // MARK: - CoreData Setup

    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "WeatherApp")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    // MARK: - Core Data Delete support

    func deleteData(city: CityMainInfo?) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                guard let results = results as? [NSManagedObject] else {return}
                for result in results {
                    if result.value(forKey: "name") as? String == city?.city
                        && result.value(forKey: "countryCode") as? String == city?.countryCode {
                        context.delete(result)
                    }
                }
            }
        } catch {}
        do {
            try context.save()
        } catch let error {
            print(error)
        }
    }

}
