//
//  NetworkManager.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 9/21/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {

    // MARK: Properties

    static let shared = NetworkManager()

    // MARK: Methods

    private init() {}

    func autocompleteListOfCities(for city: String?, transformUsingFunc: @escaping ([String]?) -> Void) {
        guard let cityName = city else {return}
        let formattedCityName = cityName.replacingOccurrences(of: " ", with: "%20")
        let urlString = "http://gd.geobytes.com/AutoCompleteCity?&template=%3Cgeobytes%20city%3E,"
            + "%20%3Cgeobytes%20iso2%3E,%20%3Cgeobytes%20country%3E&q="
            + formattedCityName
        guard let url = URL(string: urlString) else {return}
        AF.request(url).responseJSON { response in
            guard let data = response.value as? [String] else {return}
            transformUsingFunc(data)
        }
    }

    func downloadData(for city: CityMainInfo?, transformUsingFunc: @escaping ([String: Any]?, [[String: Any]]?) -> Void) {
        guard let cityName = city?.city,
            let countryCode = city?.countryCode
            else {return}
        let formattedCityName = cityName.replacingOccurrences(of: " ", with: "%20")
        let formattedCountryCode = countryCode.replacingOccurrences(of: " ", with: "%20")
        let urlString = "https://api.weatherbit.io/v2.0/current?city="
            + "\(formattedCityName),\(formattedCountryCode)&key=96523783a124407d801fd1ef731a5f8d"
        guard let url = URL(string: urlString) else {return}
        let forecastURLString = "https://api.weatherbit.io/v2.0/forecast/daily?city="
            + "\(formattedCityName),\(formattedCountryCode)&key=96523783a124407d801fd1ef731a5f8d"
        guard let forecastURL = URL(string: forecastURLString) else {return}
        AF.request(url).responseJSON { response in
            guard let currentWeather = response.value as? [String: Any] else {return}
            AF.request(forecastURL).responseJSON { forecastResponse in
                guard let forecastArray = forecastResponse.value as? [String: Any] else {return}
                let data = forecastArray["data"] as? [[String: Any]]
                guard let forecastSlice = data?[0...3] else {return}
                let forecast = Array(forecastSlice)
                DispatchQueue.main.async {
                    transformUsingFunc(currentWeather, forecast)
                }
            }
        }
    }

}
