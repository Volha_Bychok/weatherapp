//
//  Forecast+CoreDataProperties.swift
//  
//
//  Created by Ольга Бычок on 9/21/19.
//
//

import Foundation
import CoreData
import UIKit

extension Forecast {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Forecast> {
        return NSFetchRequest<Forecast>(entityName: "Forecast")
    }

    @NSManaged public var maxTemp: Double
    @NSManaged public var minTemp: Double
    @NSManaged public var code: Int16
    @NSManaged public var icon: String?
    @NSManaged public var city: City?

    }
