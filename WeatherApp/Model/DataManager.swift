//
//  DataManager.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 10/2/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation

class DataManager {

    // MARK: Properties

    private let defaultCities = [CityMainInfo(city: "Moscow", countryCode: "RU"), CityMainInfo(city: "Minsk", countryCode: "BY")]
    static var shared = DataManager()

    // MARK: Methods

    private init() {
    }

    func sendNotificationForUpdateTableView() {
        NotificationCenter.default.post(name: NSNotification.Name.init("Data has been downloaded"), object: nil)
    }

    func updateData() {
        var cities = [CityMainInfo]()
        if UserDefaults.standard.object(forKey: "First Load?") == nil {
            cities.append(contentsOf: defaultCities)
            UserDefaults.standard.set(true, forKey: "First Load?")
        } else {
            cities.append(contentsOf: PersistanceManager.shared.fetchWeatherData().map {
                CityMainInfo(city: $0.name, countryCode: $0.countryCode)
            })
        }
        for city in cities {
            createData(city: city)
        }
    }

    func createData(city: CityMainInfo) {
        NetworkManager.shared.downloadData(for: city, transformUsingFunc: { currentWeather, forecast  in
            guard let currentWeather = currentWeather,
                let forecast = forecast
                else {return}
            PersistanceManager.shared.createWeatherData(from: currentWeather, and: forecast)
            self.sendNotificationForUpdateTableView()
        })
    }
}
