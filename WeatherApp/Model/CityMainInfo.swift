//
//  City.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 9/27/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation

struct CityMainInfo: Equatable {
    let city: String?
    let countryCode: String?
}
