//
//  TableViewController.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 8/19/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class TableViewController: UITableViewController {

    // MARK: - Properties

    private let segueID = "segueID"
    private let addCitySegueID = "addingCitySegueID"
    private var cities: [City] = PersistanceManager.shared.fetchWeatherData()

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != cities.count {
            performSegue(withIdentifier: segueID, sender: indexPath.row)
        } else {
            performSegue(withIdentifier: addCitySegueID, sender: indexPath.row)
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == cities.count {
            let cellIdentifier = "addingCityCellID"
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                as? AddNewCityTableViewCell else {
                    fatalError("Can't find cell with id:\(cellIdentifier)")
            }
            if UserDefaults.standard.object(forKey: "Celsius") == nil {
                cell.setupButtons(isInitial: true)
            } else {
                cell.setupButtons(isInitial: UserDefaults.standard.bool(forKey: "Celsius"))
            }
            return cell
        } else {
            let cellIdentifier = "cellID"
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                as? WeatherTableViewCell else {
                    fatalError("Can't find cell with id:\(cellIdentifier)")
            }
            cell.updateLabelsText(with: cities[indexPath.row])
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row > 1 && indexPath.row < cities.count
    }

    override func tableView(_ tableView: UITableView,
                            commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if cities.count > indexPath.row {
            let city = cities[indexPath.row].name
            let countryCode = cities[indexPath.row].countryCode
            PersistanceManager.shared.deleteData(city: CityMainInfo(city: city, countryCode: countryCode))
            cities.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueID,
            let targetVC = segue.destination as? PageViewController {
            targetVC.cities = self.cities
            guard let index = sender as? Int else {return}
            targetVC.initialIndex = index
        }
    }

    // MARK: - Methods

    private func showNoNetworkAlertIfNeeded() {
        guard let network = NetworkReachabilityManager() else {return}
        if !network.isReachable {
            let alertController = UIAlertController(title: "No Internet Connection",
                                                    message: "Unable to display current weather", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }

    @objc private func reloadData() {
        cities = PersistanceManager.shared.fetchWeatherData()
        tableView.reloadData()
    }

    // MARK: - Overrides

    let tableRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refreshControl
    }()

    @objc private func refresh(sender: UIRefreshControl) {
        DataManager.shared.updateData()
        reloadData()
        sender.endRefreshing()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showNoNetworkAlertIfNeeded()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.refreshControl = tableRefreshControl
        DataManager.shared.updateData()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData),
                                               name: NSNotification.Name("Data has been downloaded"), object: nil)
    }

}
