//
//  CityViewController.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 8/16/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class CityViewController: UIViewController, UITableViewDataSource {

    // MARK: - Outlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!

    // MARK: - Properties
    private let cellIdentifier = "threeDayWeatherCellID"
    var index: Int = 0
    var cities: [City] = []
    var currentCityData: City?

    // MARK: - UITableViewDataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row > 4 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "detailedWeatherCellID",
                                                           for: indexPath) as? DetailedWeatherTableViewCell else {
                                                            fatalError("Can't find cell with id: detailedWeatherCellID")
            }
            cell.updateLabelsText(with: currentCityData, for: indexPath.row)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                           for: indexPath) as? ForecastTableViewCell else {
                                                            fatalError("Can't find cell with id:\(cellIdentifier)")
            }
            cell.updateLabelsText(with: currentCityData, for: indexPath.row)
            return cell
        }
    }

    // MARK: - Overrides

    override func viewDidLoad() {
        currentCityData = cities[index]
        cityLabel.text = currentCityData?.name ?? ""
        descriptionLabel.text = currentCityData?.currentWeather?.weatherDescription
        guard let temp = currentCityData?.currentWeather?.currentTemp else {return}
        temperatureLabel.text = "\(temp) °"
        currentCityData?.currentWeather?.addImage(to: imageView, elementIsIcon: false, code: currentCityData?.currentWeather?.code ?? 0)
    }

}
