//
//  PageViewController.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 8/15/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {

    // MARK: - Properties

    var cities: [City] = []
    var initialIndex: Int = 0
    private var pageControl = UIPageControl(frame: .zero)
    weak var exitButton: UIButton!

    // MARK: - Methods

    private func configureExitButton() {
        exitButton.backgroundColor = .clear
        exitButton.setTitle("☰", for: .normal)
        exitButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }

    private func configurePageControl() {
        pageControl.currentPageIndicatorTintColor = .blue
        pageControl.tintColor = .white
        pageControl.numberOfPages = cities.count
        pageControl.currentPage = initialIndex
        pageControl.hidesForSinglePage = true
        view.addSubview(pageControl)
    }

    @objc func buttonAction(sender: UIButton!) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Overrides

    override func loadView() {
        super.loadView()
        let exitButton = UIButton()
        self.view.addSubview(exitButton)
        exitButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraints([
            NSLayoutConstraint(item: exitButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 50),
            NSLayoutConstraint(item: exitButton, attribute: .height, relatedBy: .equal,
                               toItem: nil, attribute: .height, multiplier: 1.0, constant: 50),
            NSLayoutConstraint(item: exitButton, attribute: .bottom, relatedBy: .equal,
                               toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 5),
            NSLayoutConstraint(item: exitButton, attribute: .trailing, relatedBy: .equal,
                               toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: -20)
            ])
        self.exitButton = exitButton
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        configurePageControl()
        configureExitButton()
        if let viewController = getVC(at: initialIndex) {
            setViewControllers([viewController], direction: .forward, animated: true, completion: nil)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        pageControl.center = CGPoint(x: view.center.x,
                                     y: UIScreen.main.bounds.maxY - 20)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

// MARK: - UIPageViewControllerDataSource

extension PageViewController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = index(of: viewController) else {
            return nil
        }
        return getVC(at: (index - 1))
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = index(of: viewController) else {
            return nil
        }

        return getVC(at: (index + 1))
    }

    private func index(of viewController: UIViewController) -> Int? {
        return (viewController as? CityViewController)?.index
    }

    func getVC(at index: Int) -> UIViewController? {
        if index < 0 || index >= cities.count {
            return nil
        }
        let viewController = storyboard?.instantiateViewController(withIdentifier: "cityVC") as? CityViewController
        viewController?.cities = cities
        viewController?.index = index
        return viewController
    }

}

extension PageViewController: UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else { return }

        guard let currentVC = pageViewController.viewControllers?.first as? CityViewController else {
            return
        }
        pageControl.currentPage = currentVC.index
    }
}
