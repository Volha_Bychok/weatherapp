//
//  AddNewCityViewController.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 8/23/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class AddNewCityViewController: UIViewController, UISearchBarDelegate {

    // MARK: - Outlets

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Properties

    private var autocompleteResult: [String]?
    private let cellIdentifier = "autocompleteResultCell"

    // MARK: - Methods

    private func hideKeyboard() {
        self.view.endEditing(true)
    }

    func goToTableView() {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func exitButton(_ sender: UIButton) {
        goToTableView()
    }

    private func noDataAnswer() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else {return}
            strongSelf.resultLabel.text = "Sorry, there are no data"
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        hideKeyboard()
        resultLabel.text = "Please wait"
        guard let cityName = searchBar.text else {return}
        DispatchQueue.main.async {
            NetworkManager.shared.autocompleteListOfCities(for: cityName, transformUsingFunc: {[weak self] data in
                guard let self = self
                    else {return}
                self.autocompleteResult = data
                if data == ["%s"] {
                    self.noDataAnswer()
                } else {
                    self.resultLabel.isHidden = true
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }
            })
        }
    }

    // MARK: - Overrides

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideKeyboard()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        tableView.isHidden = true
    }

}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension AddNewCityViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autocompleteResult?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AutocompleteResultTableViewCell else {
            fatalError("Can't find cell with id:\(cellIdentifier)")
        }
        let info = autocompleteResult?[indexPath.row].components(separatedBy: ", ")
        cell.info = info
        cell.updateInfo()
        cell.delegate = self
        return cell
    }

}
