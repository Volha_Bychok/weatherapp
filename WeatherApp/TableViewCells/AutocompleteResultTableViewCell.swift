//
//  AutocompleteResultTableViewCell.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 9/27/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import CoreData

class AutocompleteResultTableViewCell: UITableViewCell {

    // MARK: Properties

    weak var delegate: AddNewCityViewController?
    var info: [String]?

    // MARK: Outlets

    @IBOutlet weak var titleLabel: UILabel!

    // MARK: Methods

    @IBAction func cellButtonTapped(_ sender: UIButton) {
        goToTableView()
    }

    func updateInfo() {
        guard let city = info?.first,
            let country = info?.last
            else {return}
        titleLabel?.text = "\(city), \(country)"
    }

    func goToTableView() {
        guard let info = info
            else {return}
        if info.count >= 2 {
            let cityName = info.first
            let countryCode = info[1]
            let city = CityMainInfo(city: cityName, countryCode: countryCode)
            DataManager.shared.createData(city: city)
            delegate?.goToTableView()
        }
    }

}
