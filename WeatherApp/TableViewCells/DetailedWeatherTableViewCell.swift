//
//  DetailedWeatherTableViewCell.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 9/9/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class DetailedWeatherTableViewCell: UITableViewCell {

    // MARK: - Outlets

    @IBOutlet weak var upperLeftLabel: UILabel!
    @IBOutlet weak var upperRightLabel: UILabel!
    @IBOutlet weak var lowerLeftLabel: UILabel!
    @IBOutlet weak var lowerRightLabel: UILabel!

    // MARK: - Methods

    func updateLabelsText(with data: City?, for index: Int) {
        guard let data = data
            else {return}
        let currentData = data.currentWeather
        guard let windKph = currentData?.windSpeed,
            let cloud = currentData?.cloud,
            let humidity = currentData?.humidity,
            let feelslike = currentData?.feelsLike,
            let pressure = currentData?.pressure,
            let precipitation = currentData?.precipitation,
            let visibility = currentData?.visibility,
            let uvIndex = currentData?.uvIndex,
            let windDir = currentData?.windDirection,
            let sunrise = data.currentWeather?.sunrise,
            let sunset = data.currentWeather?.sunset
            else {return}
        let format = DateFormatter()
        format.dateFormat = "HH:mm"
        format.timeZone = TimeZone(abbreviation: "UTC")
        let sunriseTime = format.date(from: sunrise)
        let sunsetTime = format.date(from: sunset)
        format.dateFormat = "hh:mm a"
        guard let timezone = data.timeZone,
            let sunriseTimeUnwrapped = sunriseTime,
            let sunsetTimeUnwrapped = sunsetTime
            else {return}
        format.timeZone = TimeZone(identifier: timezone)
        let sunriseLocal = format.string(from: (sunriseTimeUnwrapped))
        let sunsetLocal = format.string(from: (sunsetTimeUnwrapped))
        switch index {
        case 5:
            upperLeftLabel.text = "SUNRISE"
            upperRightLabel.text = "SUNSET"
            lowerLeftLabel.text = sunriseLocal
            lowerRightLabel.text = sunsetLocal
        case 6:
            upperLeftLabel.text = "CLOUDS"
            upperRightLabel.text = "HUMIDITY"
            lowerLeftLabel.text = "\(cloud) %"
            lowerRightLabel.text = "\(humidity)"
        case 7:
            upperLeftLabel.text = "WIND"
            upperRightLabel.text = "FEELS LIKE"
            lowerLeftLabel.text = "\(windDir) \(Int(windKph))"
            lowerRightLabel.text = String(Int(feelslike))
        case 8:
            upperLeftLabel.text = "PRECIPITATION"
            upperRightLabel.text = "PRESSURE"
            lowerLeftLabel.text = "\(precipitation * 10) cm"
            lowerRightLabel.text = "\(Int(pressure)) hPa"
        case 9:
            upperLeftLabel.text = "VISIBILITY"
            upperRightLabel.text = "UV INDEX"
            lowerLeftLabel.text = "\(Int(visibility)) km"
            lowerRightLabel.text = "\(uvIndex)"
        default:
            print("not existed")
        }
    }

}
