//
//  WeatherTableViewCell.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 8/19/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    // MARK: - Outlets

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var imageWeatherView: UIImageView!

    // MARK: - Methods

    func updateLabelsText(with data: City) {
        let today = Date()
        let format = DateFormatter()
        guard let timezone = data.timeZone else {return}
        format.timeZone = TimeZone.init(identifier: timezone)
        format.dateFormat = "h:mm a"
        let dateString = format.string(from: today)
        timeLabel.text = dateString
        titleLabel.text = data.name ?? ""
        guard let temp = data.currentWeather?.currentTemp else {return}
        detailLabel.text = "\(temp) °"
        data.currentWeather?.addImage(to: imageWeatherView, elementIsIcon: false, code: data.currentWeather?.code ?? 0)
    }

}
