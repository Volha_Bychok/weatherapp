//
//  AddNewCityTableViewCell.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 10/13/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class AddNewCityTableViewCell: UITableViewCell {

    @IBOutlet weak var addNewCityLabel: UILabel!
    @IBOutlet weak var chooseCelsiusButton: UIButton!
    @IBOutlet weak var chooseFahrenheitButton: UIButton!

    @IBAction func chooseCelsiusButton(_ sender: UIButton) {
        setupButtons(isInitial: true)
        UserDefaults.standard.set(true, forKey: "Celsius")
        PersistanceManager.shared.changeWeatherTemperatureData()
    }

    @IBAction func chooseFahrenheitButton(_ sender: UIButton) {
        setupButtons(isInitial: false)
        UserDefaults.standard.set(false, forKey: "Celsius")
        PersistanceManager.shared.changeWeatherTemperatureData()
    }

    func setupButtons(isInitial: Bool) {
        chooseCelsiusButton.isSelected = isInitial
        chooseFahrenheitButton.isSelected = !isInitial
    }

}
