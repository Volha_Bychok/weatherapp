//
//  ForecastTableViewCell.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 9/6/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {

    // MARK: - Outlets

    @IBOutlet weak var mainInfoLabel: UILabel!
    @IBOutlet weak var dayNameLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var weatherIconImageView: UIImageView!

    // MARK: - Properties

    let today = Date()

    // MARK: - Methods

    func updateLabelsText(with currentCityData: City?, for index: Int) {
        guard let currentCityData = currentCityData
            else {return}
        var data: Forecast?
        if index == 4 || index == 0 {
            data = currentCityData.forecasts?.firstObject as? Forecast
        } else {
            data = currentCityData.forecasts?[index] as? Forecast
        }
        guard let maxTemp = data?.maxTemp,
            let desc = currentCityData.currentWeather?.weatherDescription,
            let minTemp = data?.minTemp
            else {return}
        if index == 4 {
            mainInfoLabel.text =
            "Today: \(desc) currently. The high will be \(maxTemp). Tonight is a low of \(minTemp)"
        } else {
            maxTempLabel.text = String(maxTemp)
            minTempLabel.text = String(minTemp)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            guard let forecastDay = Calendar.current.date(byAdding: .day, value: index, to: today) else {return}
            let dayName = dateFormatter.string(from: (forecastDay))
            if index == 0 {
                dayNameLabel.text = "\(dayName)   today"
            } else {

                dayNameLabel.text = dayName
            }
            currentCityData.currentWeather?.addImage(to: weatherIconImageView, elementIsIcon: true, code: data?.code ?? 0)
        }
    }

}
