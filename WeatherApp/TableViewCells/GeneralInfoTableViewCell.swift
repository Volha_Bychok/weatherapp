//
//  GeneralInfoTableViewCell.swift
//  WeatherApp
//
//  Created by Ольга Бычок on 9/9/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class GeneralInfoTableViewCell: UITableViewCell {

    // MARK: - Outlets

    @IBOutlet weak var generalInfoLabel: UILabel!

}
