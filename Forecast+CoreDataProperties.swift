//
//  Forecast+CoreDataProperties.swift
//  
//
//  Created by Ольга Бычок on 10/1/19.
//
//

import Foundation
import CoreData

extension Forecast {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Forecast> {
        return NSFetchRequest<Forecast>(entityName: "Forecast")
    }

    @NSManaged public var code: Int64
    @NSManaged public var maxTemp: Int64
    @NSManaged public var minTemp: Int64
    @NSManaged public var city: City?

}
